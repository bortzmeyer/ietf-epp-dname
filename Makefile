# TODO check the draft itself?
check:
	 xmllint --noout --schema http://www.w3.org/2001/XMLSchema.xsd epp-schema.xsd

all: check draft

draft: draft-bortzmeyer-regext-epp-dname.txt

draft-bortzmeyer-regext-epp-dname.txt: draft-bortzmeyer-regext-epp-dname.xml
	xml2rfc draft-bortzmeyer-regext-epp-dname.xml

draft-bortzmeyer-regext-epp-dname.xml: draft-bortzmeyer-regext-epp-dname-noschema.xml epp-schema.xsd
	sed "/^BEGIN$$/r epp-schema.xsd" draft-bortzmeyer-regext-epp-dname-noschema.xml > $@ || rm -f $@

clean:
	rm -f draft-bortzmeyer-regext-epp-dname.xml draft-bortzmeyer-regext-epp-dname.txt
